package pl.sdacademy;

import pl.sdacademy.zadania.Zad2_SimpleReader;

import java.io.IOException;

public class App
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println(Zad2_SimpleReader.readSimpleExample());
    }
}
