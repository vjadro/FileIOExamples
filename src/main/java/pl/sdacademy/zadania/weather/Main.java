package pl.sdacademy.zadania.weather;

import pl.sdacademy.zadania.weather.model.DayData;
import pl.sdacademy.zadania.weather.parser.WeatherDataParser;
import pl.sdacademy.zadania.weather.service.WeatherService;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<DayData> dayDataList = WeatherDataParser.getDataFromFile();
        WeatherService service = new WeatherService(dayDataList);

        DayData day1 = service.getDayByDate(LocalDate.parse("2000-10-10"));
        System.out.println(day1);

        for(DayData day : service.getRangeOfDays(LocalDate.parse("2000-10-10"), LocalDate.parse("2000-10-14"))) {
            System.out.println(day);
        }

        System.out.println(service.getMinTempInRange(LocalDate.parse("2000-10-10"), LocalDate.parse("2000-10-14")));
        System.out.println(service.getMaxTempInRange(LocalDate.parse("2000-10-10"), LocalDate.parse("2000-10-14")));
        System.out.println(service.getAvgTempInRange(LocalDate.parse("2000-10-10"), LocalDate.parse("2000-10-14")));

        System.out.println("Liczba dni ze średnią temperaturą >= 20:");
        System.out.println(service.getNumberOfDaysWithHigherAvg(20));

        System.out.println(service.findHottestAndCoolestYear());
    }
}
