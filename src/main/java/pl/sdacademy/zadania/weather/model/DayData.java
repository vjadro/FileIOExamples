package pl.sdacademy.zadania.weather.model;

import java.time.LocalDate;

public class DayData {
    private LocalDate date;
    private int maxTemp;
    private int meanTemp;
    private int minTemp;

    public DayData(LocalDate date, int maxTemp, int meanTemp, int minTemp) {
        this.date = date;
        this.maxTemp = maxTemp;
        this.meanTemp = meanTemp;
        this.minTemp = minTemp;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public int getMeanTemp() {
        return meanTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    @Override
    public String toString() {
        return "DayData{" +
                "date=" + date +
                ", maxTemp=" + maxTemp +
                ", meanTemp=" + meanTemp +
                ", minTemp=" + minTemp +
                '}';
    }
}
