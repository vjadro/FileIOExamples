package pl.sdacademy.zadania.weather.service;

import pl.sdacademy.zadania.weather.model.DayData;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class WeatherService {

    private List<DayData> dayDataList;

    public WeatherService(List<DayData> dayDataList) {
        this.dayDataList = dayDataList;
    }

    public DayData getDayByDate(LocalDate date) {
        for (DayData day : dayDataList) {
            if (day.getDate().isEqual(date))
                return day;
        }

        return null;
    }

    // is date after day.date?
    private boolean isDateAfter(DayData day, LocalDate date) {
        return date.isAfter(day.getDate());
    }

    //is date before day.date?
    private boolean isDateBefore(DayData day, LocalDate date) {
        return date.isBefore(day.getDate());
    }

    //is date equal to day.date?
    private boolean isDateEqual(DayData day, LocalDate date) {
        return date.isEqual(day.getDate());
    }

    //is day.date in date range?
    private boolean isDayInRange(DayData day, LocalDate dateFrom, LocalDate dateTo) {
        return (isDateAfter(day, dateFrom) && isDateBefore(day, dateTo)) ||
                isDateEqual(day, dateFrom) || isDateEqual(day, dateTo);
    }

    public List<DayData> getRangeOfDays(LocalDate dateFrom, LocalDate dateTo) {
        List<DayData> listOfDays = new ArrayList<>();

        if (dateFrom.isAfter(dateTo))
            return listOfDays;

        for (DayData day : dayDataList) {

            if (isDayInRange(day, dateFrom, dateTo))
                listOfDays.add(day);
        }

        return listOfDays;
    }

    public int getMinTempInRange(LocalDate dateFrom, LocalDate dateTo) {
        int min = Integer.MAX_VALUE;

        for (DayData day : getRangeOfDays(dateFrom, dateTo)) {
            if (day.getMinTemp() < min)
                min = day.getMinTemp();
        }

        return min;
    }

    public int getMaxTempInRange(LocalDate dateFrom, LocalDate dateTo) {
        int max = Integer.MIN_VALUE;

        for (DayData day : getRangeOfDays(dateFrom, dateTo)) {
            if (day.getMaxTemp() > max)
                max = day.getMaxTemp();
        }

        return max;
    }

    public float getAvgTempInRange(LocalDate dateFrom, LocalDate dateTo) {
        int sum = 0;

        List<DayData> days = getRangeOfDays(dateFrom, dateTo);

        for (DayData day : days) {
            sum += day.getMeanTemp();
        }

        return (float) sum / days.size();
    }

    public int getNumberOfDaysWithHigherAvg(int temp) {

        int total = 0;

        for (DayData day : dayDataList) {
            if (day.getMeanTemp() >= temp)
                total++;
        }

        return total;
    }

    public String findHottestAndCoolestYear() {

        int startYear = dayDataList.get(0).getDate().getYear();
        int endYear = dayDataList.get(dayDataList.size() - 1).getDate().getYear();

        float lowestTemperature = Float.MAX_VALUE;
        float highestTemperature = Float.MIN_VALUE;

        int lowYear = 0;
        int highYear = 0;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");

        for (int i = startYear; i <= endYear; i++) {
            float yearAvg = getAvgTempInRange(LocalDate.parse(i + "-1-1", formatter), LocalDate.parse(i + "-12-31", formatter));

            if (yearAvg < lowestTemperature) {
                lowestTemperature = yearAvg;
                lowYear = i;
            }
            if (yearAvg > highestTemperature) {
                highestTemperature = yearAvg;
                highYear = i;
            }
        }

        return "Rok " + lowYear + " był najzimniejszy (" + lowestTemperature + ")\n" +
                "Rok " + highYear + " był najcieplejszy (" + highestTemperature + ")";
    }

    public String findHottestAndCoolestMonth() {

        float lowMonth = Float.MAX_VALUE;
        float highMonth = Float.MIN_VALUE;

        int lowMonthId = -1;
        int highMonthId = -1;


        return "";
    }
}
