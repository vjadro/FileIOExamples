package pl.sdacademy.zadania.weather.parser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import pl.sdacademy.zadania.weather.model.DayData;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class WeatherDataParser {
    private static final String FILE_PATH = "resources/weather-data.csv";
    private static final int INDEX_DATE = 0;
    private static final int INDEX_MAX_TEMP = 1;
    private static final int INDEX_MEAN_TEMP = 2;
    private static final int INDEX_MIN_TEMP = 3;
    public static int errors = 0;

    public static List<DayData> getDataFromFile() {
        List<DayData> dayDataList = new ArrayList<>();

        LineIterator fileContents = null;

        try {
            fileContents = FileUtils.lineIterator(new File(FILE_PATH), "UTF-8");
        } catch (IOException e) {
            return dayDataList;
        }

        if (fileContents.hasNext())
            fileContents.nextLine();

        while(fileContents.hasNext()) {
            String[] line = fileContents.nextLine().split(",");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
            try {
                LocalDate date = LocalDate.parse(line[INDEX_DATE], formatter);
                int max = Integer.parseInt(line[INDEX_MAX_TEMP]);
                int mean = Integer.parseInt(line[INDEX_MEAN_TEMP]);
                int min = Integer.parseInt(line[INDEX_MIN_TEMP]);

                dayDataList.add(new DayData(date,max,mean,min));

            } catch(DateTimeParseException e) {
                errors++;
            } catch(NumberFormatException e) {
                errors++;
            }
        }

        return dayDataList;
    }
}
