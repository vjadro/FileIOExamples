package pl.sdacademy.zadania.zad4.parser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import pl.sdacademy.zadania.zad4.model.Book;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BooksParser {

    private static final int INDEX_ID = 0;
    private static final int INDEX_CAT = 1;
    private static final int INDEX_NAME = 2;
    private static final int INDEX_PRICE = 3;
    private static final int INDEX_INSTOCK = 4;
    private static final int INDEX_AUTHOR = 5;
    private static final int INDEX_SERIES = 6;
    private static final int INDEX_SEQUENCE = 7;
    private static final int INDEX_GENRE = 8;

    public static List<Book> getBooksFromFile(File file) {
        List<Book> books = new ArrayList<>();

        LineIterator fileContents = null;

        try {
            fileContents = FileUtils.lineIterator(file, "UTF-8");
        } catch (IOException e) {
            return books;
        }

        if (fileContents.hasNext())
            fileContents.nextLine();

        while(fileContents.hasNext()) {
            String[] line = fileContents.nextLine().split(",");

            String id = line[INDEX_ID];
            String cat = line[INDEX_CAT];
            String name = line[INDEX_NAME];
            float price = Float.parseFloat(line[INDEX_PRICE]);
            boolean inStock = Boolean.parseBoolean(line[INDEX_INSTOCK]);
            String author = line[INDEX_AUTHOR];
            String series = line[INDEX_SERIES];
            int sequence = Integer.parseInt(line[INDEX_SEQUENCE]);
            String genre = line[INDEX_GENRE];

            books.add(new Book(id,cat,name,price,inStock,author,series,sequence,genre));
        }

        return books;
    }

    public static void saveBooksToFile(List<Book> books) throws IOException {
        List<String> lines = new ArrayList<>();

        lines.add("id,cat,name,price,inStock,author_t,series_t,sequence_i,genre_s");
        for(Book book : books) {
            String line = book.getId() + "," +
                    book.getCat() + "," +
                    book.getName() + "," +
                    book.getPrice() + "," +
                    book.isInStock() + "," +
                    book.getAuthor() + "," +
                    book.getSeries() + "," +
                    book.getSequence() + "," +
                    book.getGenre();

            lines.add(line);
        }

        FileUtils.writeLines(new File("resources/sortedBooks.csv"), lines);
    }
}
