package pl.sdacademy.zadania.zad4.model;

import java.util.Comparator;

public class Book implements Comparable<Book> {

    private String id;
    private String cat;
    private String name;
    private float price;
    private boolean inStock;
    private String author;
    private String series;
    private int sequence;
    private String genre;

    public Book(String id, String cat, String name, float price, boolean inStock, String author, String series, int sequence, String genre) {
        this.id = id;
        this.cat = cat;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.author = author;
        this.series = series;
        this.sequence = sequence;
        this.genre = genre;
    }

    public String getId() {
        return id;
    }

    public String getCat() {
        return cat;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public boolean isInStock() {
        return inStock;
    }

    public String getAuthor() {
        return author;
    }

    public int getSequence() {
        return sequence;
    }

    public String getGenre() {
        return genre;
    }

    public String getSeries() {
        return series;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", cat='" + cat + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", inStock=" + inStock +
                ", author='" + author + '\'' +
                ", series='" + series + '\'' +
                ", sequence=" + sequence +
                ", genre='" + genre + '\'' +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        int start = compareStock(o.isInStock(), inStock);

        return start != 0 ? start : comparePrice(o.getPrice(), price);
    }

    private static int compareStock(boolean a, boolean b) {
        return a && !b ? 1
                : !a && b ? -1
                : 0;
    }

    private static int comparePrice(float a, float b) {
        return Float.compare(b, a);
    }
}
