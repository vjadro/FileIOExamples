package pl.sdacademy.zadania.zad4;

import pl.sdacademy.zadania.zad4.model.Book;
import pl.sdacademy.zadania.zad4.parser.BooksParser;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {

        List<Book> books = BooksParser.getBooksFromFile(new File("resources/books.csv"));

        Collections.sort(books);

        for(Book book : books) {
            System.out.println(book.isInStock() + "\t" + book.getPrice());
        }

        BooksParser.saveBooksToFile(books);
    }
}
