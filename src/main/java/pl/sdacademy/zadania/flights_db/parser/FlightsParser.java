package pl.sdacademy.zadania.flights_db.parser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import pl.sdacademy.zadania.flights_db.model.Flight;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlightsParser {
    public static final int FLIGHT_YEAR = 0;
    public static final int FLIGHT_MONTH = 1;
    public static final int FLIGHT_PASSENGERS = 2;
    public static final String FLIGHTS_FILE_PATH = "resources/flights.csv";

    public static List<Flight> getFlightsList() {
        List<Flight> flights = new ArrayList<>();

        LineIterator fileContents = null;

        try {
            fileContents = FileUtils.lineIterator(new File(FLIGHTS_FILE_PATH), "UTF-8");
        } catch (IOException e) {
            return flights;
        }

        if (fileContents.hasNext())
            fileContents.nextLine();

        while(fileContents.hasNext()) {
            String[] line = fileContents.nextLine().split(",");

            String month = line[FLIGHT_MONTH];
            int year, passengers;

            try {
                year = Integer.parseInt(line[FLIGHT_YEAR]);
                passengers = Integer.parseInt(line[FLIGHT_PASSENGERS]);
                flights.add(new Flight(year,month,passengers));
            } catch(NumberFormatException e) {
                System.out.println("Parsing error.");
            }
        }

        return flights;
    }

    public static void saveToFile(String fileName, List<String> lines) {
        try {
            FileUtils.writeLines(new File("resources/" + fileName), lines);
        } catch (IOException e) {
            System.out.println("Couldn't create file!");
        }
    }
}
