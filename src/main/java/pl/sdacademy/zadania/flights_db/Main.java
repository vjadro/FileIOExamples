package pl.sdacademy.zadania.flights_db;

import pl.sdacademy.zadania.flights_db.model.Flight;
import pl.sdacademy.zadania.flights_db.parser.FlightsParser;
import pl.sdacademy.zadania.flights_db.service.FlightsService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FlightsService flightsService = new FlightsService();

        for(Flight flight : FlightsParser.getFlightsList()) {
            flightsService.addFlight(flight);
        }

        List<String> lines = new ArrayList<>();

        lines.add("Total passengers => " + flightsService.getAllPassengersNumber());

        lines.add("Passengers by year:");

        for(String line : flightsService.getPassengerByYear()) {
            lines.add(line);
        }

        lines.add("Passengers by month:");

        for(String line : flightsService.getPassengersByMonth()) {
            lines.add(line);
        }

        lines.add(flightsService.getMinMaxPassengers());

        FlightsParser.saveToFile("fligthsAnalitics.txt",lines);
    }
}
