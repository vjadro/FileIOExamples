package pl.sdacademy.zadania.flights_db.service;

import pl.sdacademy.zadania.flights_db.model.Flight;

import java.util.*;

public class FlightsService {
    private List<Flight> flights = new ArrayList<>();

    public void addFlight(Flight flight) {
        flights.add(flight);
    }

    public int getAllPassengersNumber() {
        int sum = 0;

        for (Flight flight : flights) {
            sum += flight.getPassengers();
        }

        return sum;
    }

    public List<String> getPassengerByYear() {
        List<String> result = new ArrayList<>();

        int currentYear = flights.get(0).getYear();
        int currentYearPassengers = 0;

        for(Flight flight : flights) {
            if (currentYear == flight.getYear()) {
                currentYearPassengers += flight.getPassengers();
            } else {
                String line = currentYear + " => " + currentYearPassengers + " passengers";
                result.add(line);
                currentYear = flight.getYear();
                currentYearPassengers = 0;
            }
        }

        return result;
    }

    public List<String> getPassengersByMonth() {
        List<String> result = new ArrayList<>();

        Map<String, Integer> monthsMap = new HashMap<>();

        for(Flight flight : flights) {
            String month = flight.getMonth();

            if (monthsMap.containsKey(month)) {
                int passengers = monthsMap.get(month) + flight.getPassengers();
                monthsMap.replace(month, passengers);
            } else {
                monthsMap.put(month, flight.getPassengers());
            }
        }

        Iterator<String> iterator = monthsMap.keySet().iterator();
        while(iterator.hasNext()) {
            String month = iterator.next();
            int passengers = monthsMap.get(month);

            result.add(month + " => " + passengers);
        }

        return result;
    }

    public String getMinMaxPassengers() {
        Flight minPassengerFlight = flights.get(0), maxPassengerFlight = flights.get(0);

        for(Flight flight : flights) {
            if (flight.getPassengers() < minPassengerFlight.getPassengers())
                minPassengerFlight = flight;
            if (flight.getPassengers() > maxPassengerFlight.getPassengers())
                maxPassengerFlight = flight;
        }

        return "Lowest number of passengers at " +
                minPassengerFlight.getMonth() + ", " + minPassengerFlight.getYear() +
                " => " + minPassengerFlight.getPassengers() + "\n" +
                "Highest number of passengers at " +
                maxPassengerFlight.getMonth() + ", " + maxPassengerFlight.getYear() +
                " => " + maxPassengerFlight.getPassengers();
    }
}
