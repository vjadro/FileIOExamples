package pl.sdacademy.zadania.flights_db.model;

public class Flight {
    private int year;
    private String month;
    private int passengers;

    public Flight(int year, String month, int passengers) {
        this.year = year;
        this.month = month;
        this.passengers = passengers;
    }

    public int getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public int getPassengers() {
        return passengers;
    }
}
