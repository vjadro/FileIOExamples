package pl.sdacademy.zadania;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Zad2_SimpleReader {
    public static String readSimpleExample() throws IOException {
        StringBuilder result = new StringBuilder();

        File file = new File("resources/simpleExample.txt");

        FileReader reader = new FileReader(file);

        String line = "";

        BufferedReader bfr = new BufferedReader(reader);

        while ((line = bfr.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }
}
