package pl.sdacademy.zadania.zad3.loader;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import pl.sdacademy.zadania.zad3.model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserLoader {

    public static List<User> getUsersFromFile(File file) {

        List<User> users = new ArrayList<>();

        LineIterator fileContents = null;

        try {
            fileContents = FileUtils.lineIterator(file, "UTF-8");
        } catch (IOException e) {
            return users;
        }

        while(fileContents.hasNext()) {
            String[] line = fileContents.nextLine().split(" ");

            int age = Integer.parseInt(line[2]);

            users.add(new User(line[0], line[1], age));
        }

        return users;
    }
}
