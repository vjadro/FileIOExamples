package pl.sdacademy.zadania.zad3;

import pl.sdacademy.zadania.zad3.loader.UserLoader;
import pl.sdacademy.zadania.zad3.model.User;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> users = UserLoader.getUsersFromFile(new File("resources/users.txt"));

        List<User> maleUsers = new ArrayList<>();
        List<User> femaleUsers = new ArrayList<>();

        for(User user : users) {
            if (user.getAge() >= 18) {
                if (user.getFirstName().charAt(user.getFirstName().length()-1) == 'a') {
                    femaleUsers.add(user);
                } else {
                    maleUsers.add(user);
                }
            }
        }

        System.out.println(maleUsers);
        System.out.println(femaleUsers);
    }
}
