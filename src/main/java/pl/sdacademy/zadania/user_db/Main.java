package pl.sdacademy.zadania.user_db;

import pl.sdacademy.zadania.user_db.parser.UserActionsParser;
import pl.sdacademy.zadania.user_db.service.UserService;

import java.util.List;

public class Main {

    public static final int USER_ACTION = 0;
    public static final int USER_LOGIN = 1;
    public static final int USER_PASSWORD = 2;
    public static final int USER_FNAME = 3;
    public static final int USER_LNAME = 4;
    public static final int USER_ROLE = 5;

    public static void main(String[] args) {

        List<String> actions = UserActionsParser.readLines();
        UserService userService = new UserService();

        for(String line : actions) {
            String[] action = line.split(";",-1);

            switch(action[USER_ACTION]) {
                case "CREATE":
                    userService.addUser(action[USER_LOGIN],action[USER_PASSWORD],action[USER_FNAME],
                                        action[USER_LNAME], action[USER_ROLE]);
                    break;
                case "UPDATE":
                    userService.updateUser(action[USER_LOGIN],action[USER_PASSWORD],action[USER_FNAME],
                            action[USER_LNAME], action[USER_ROLE]);
                    break;
                case "REMOVE":
                    userService.removeUser(action[USER_LOGIN]);
                    break;
            }
        }

        System.out.println(userService.getUser("dtrump"));
        System.out.println(userService.getUser("mamajchrzak"));
    }

}
