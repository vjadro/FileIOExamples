package pl.sdacademy.zadania.user_db.service;

import pl.sdacademy.zadania.user_db.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserService {
    private Map<String, User> users = new HashMap<>();

    public void addUser(String login, String password, String fName, String lName, String role)  {
        users.put(login, new User(login, password, fName, lName, role));
    }

    public void updateUser(String login, String password, String fName, String lName, String role) {
        User user = users.get(login);
        if (!password.equals(""))
            user.setPassword(password);
        if (!fName.equals(""))
            user.setfName(fName);
        if (!lName.equals(""))
            user.setlName(lName);
        if (!role.equals(""))
            user.setRole(role);
        users.replace(login, user);
    }

    public void removeUser(String login) {
        users.remove(login);
    }

    public User getUser(String login) {
        return users.get(login);
    }
}
