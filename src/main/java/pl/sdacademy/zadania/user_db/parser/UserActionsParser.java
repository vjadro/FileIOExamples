package pl.sdacademy.zadania.user_db.parser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserActionsParser {

    public static List<String> readLines() {
        List<String> lines = new ArrayList<>();

        LineIterator fileContents = null;

        try {
            fileContents = FileUtils.lineIterator(new File("resources/userActions.csv"), "UTF-8");
        } catch (IOException e) {
            return lines;
        }

        if (fileContents.hasNext())
            fileContents.nextLine();

        while(fileContents.hasNext()) {
            lines.add(fileContents.nextLine());
        }

        return lines;
    }
}
